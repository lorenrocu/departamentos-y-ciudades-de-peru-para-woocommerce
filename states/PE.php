<?php
/**
 * Perú states
 *
 * @author   Lorenzo Romero Cubas <lorenrocu@gmail.com>
 * @version  1.0.0
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 */

global $states;

$states ['PE' ] = array (
	'AMA' => 'Amazonas',
	'ANC' => 'Ancash',
	'APU' => 'Apurímac',
	'ARE' => 'Arequipa',
	'AYA' => 'Ayacucho',
	'CAJ' => 'Cajamarca',
	'CAL' => 'Callao',
	'CUS' => 'Cusco',
	'HUC' => 'Huánuco',
	'HUV' => 'Huancavelica',
	'ICA' => 'Ica',
	'JUN' => 'Junín',
	'LAL' => 'La Libertad',
	'LAM' => 'Lambayeque',
	'LIM' => 'Lima',
	'LMA' => 'Municipalidad Metropolitana de Lima',
	'LOR' => 'Loreto',
	'MDD' => 'Madre de Dios',
	'MOQ' => 'Moquegua',
	'PAS' => 'Pasco',
	'PIU' => 'Piura',
	'PUN' => 'Puno',
	'SAM' => 'San Martín',
	'TAC' => 'Tacna',
	'TUM' => 'Tumbes',
	'UCA' => 'Ucayali',
);