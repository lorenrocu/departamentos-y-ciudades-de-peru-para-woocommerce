=== Departamentos y Ciudades de Perú para Woocommerce ===
Contributors: @lorenrocu - Lorenzo Romero
Donate link: http://caxasweb.net.pe
Tags: woocommerce, Perú, departamentos, ciudades, states cities,woocommerce departamentos de Perú, woocommerce ciudades de Perú, desplegable, departamentos desplegables, ciudades desplegables, city dropdown, state dropdown, city select, cities select,
seleccionar ciudades,seleccionar departamentos
Requires at least: 4.6
Tested up to: 5.1
Stable tag: 1.0.0
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Wordpress plugin that shows dropdowns for State and City Select for woocomerce

== Description ==

This WooCommerce plugin transforms the text input for states, the city or town. With this plugin you can provide a list of states and cities to be shown as a select dropdown.

This will be shown in checkout pages, edit addresses pages, shipping calculator, etc.

= Supported Countries =
 * Perú

== Installation ==

= Minimum Requirements =

WordPress 4.0  or greater
Woocommerce 2.2 or greater
PHP version 5.2.4 or greater
MySQL version 5.0 or greater

= Automatic installation =

- Automatic installation is the easiest option as WordPress handles the file transfers itself and you don’t need to leave your web browser. To do an automatic install of WooCommerce, log in to your WordPress dashboard, navigate to the Plugins menu and click `Add New`.
- Search for "Departamentos y Ciudades de Perú para Woocommerce", install and activate.
- Available [@Github](https://gitlab.com/lorenrocu/departamentos-y-ciudades-de-peru-para-woocommerce).


= Manual installation =

[See wordpress codex](http://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation)


= Updating =


Automatic updates should work like a charm; as is the best practice, back up should be undertaken before updates.

If on the off-chance you do encounter issues with the shop/category pages after an update you simply need to flush the permalinks by going to WordPress > Settings > Permalinks and hitting 'save'. That should return things to normal.


This section describes how to install the plugin and get it working.

e.g.

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Use the Settings->Plugin Name screen to configure the plugin
1. (Make your instructions match the desired user flow for activating and installing your plugin. Include any steps that might be needed for explanatory purposes)


== Frequently Asked Questions ==

= It includes all the departments?

Yes, it includes all departments and provinces.

= Does it include the districts?

Only includes districts of Callao and Lima

= Something else you have not told me?

* In the shipping area so you can impose rules you must first add the department in the field of states

== Changelog ==
= 1.0 =
* 17/05/2019 First release.